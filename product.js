const section = document.getElementById('product-section')
const loader = document.getElementById('loader')
const logOutButton = document.getElementById('log-out')
const userName = document.getElementById('user-name')

logOutButton.addEventListener('click', () => {
    localStorage.removeItem('isLogIn')
    window.location.href = 'login.html'
})

document.addEventListener('DOMContentLoaded', () => {
    const itemID = localStorage.getItem("id")
    if (localStorage.getItem('isLogIn')) {
        const logInData = localStorage.getItem('isLogIn')
        const logInInfo = JSON.parse(logInData)
        if (logInInfo.isLogIn === false) {
            window.location.href = 'login.html'
            return
        } else {
            const userData = JSON.parse(localStorage.getItem(logInInfo.email))
            userName.innerText = userData.name
        }
    } else {
        window.location.href = 'login.html'
        return
    }
    document.body.style.display='block'
    loader.style.display='block'
    fetch(`https://fakestoreapi.com/products/${itemID}`)
        .then((response) => {
            return response.json()
        })
        .then((result) => {
            const product = document.createElement('div')
            const productDetail = document.createElement('div')
            const productTitle = document.createElement('h2')
            const productImage = document.createElement('img')
            const productDescription = document.createElement('p')
            const productPrice = document.createElement('p')
            const productRating = document.createElement('p')
            const productReviewCount = document.createElement('p')

            product.setAttribute('id', 'product-container')
            productDetail.setAttribute('id','product-detail')
            productImage.setAttribute('src', `${result.image}`)
            productImage.setAttribute('alt', 'product-image')
            productTitle.setAttribute('id', 'title')
            productDescription.setAttribute('id', 'description')
            productPrice.setAttribute('id', 'price')
            productRating.setAttribute('id', 'rating')
            productReviewCount.setAttribute('id', 'count')

            productTitle.innerText = result.title
            productDescription.innerText = result.description
            productPrice.innerText = `Price: $` + result.price
            productRating.innerText = `Rating:` + result.rating.rate
            productReviewCount.innerText = `Number of Reviews:` + result.rating.count

            product.append(productImage)
            productDetail.append(productTitle)
            productDetail.append(productPrice)
            productDetail.append(productRating)
            productDetail.append(productReviewCount)
            productDetail.append(productDescription)
            product.append(productDetail)
            section.append(product)
            loader.style.display='none'

        }).catch((err) => {
            const errBody = document.createElement('h1')
            errBody.setAttribute('id', 'error')
            errBody.innerText = err.message
            loader.style.display = 'none'
            section.append(errBody)
    
        })
})

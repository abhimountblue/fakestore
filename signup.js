const form = document.getElementById('form')

window.addEventListener('load', () => {
    if (localStorage.getItem('isLogIn')) {
        const logInData = localStorage.getItem('isLogIn')
        const logInInfo = JSON.parse(logInData)
        if (logInInfo.isLogIn === true) {
            window.location.href = 'index.html'
            return
        }
    }
})

form.addEventListener('submit', (event) => {
    event.preventDefault()
    const confirmPassword = document.getElementById('confirm-password').value
    const password = document.getElementById('password').value
    const email = document.getElementById('email').value
    const name = document.getElementById('name').value
    const formError = document.getElementById('form-error')
    formError.innerText = ''
    formError.style.display = 'none'
    formError.style.fontSize = '100%'

    const nameFormat = /^[a-zA-Z ]+$/
    if (name.length === 0) {
        formError.innerText = 'Name is required'
        formError.style.display = 'block'
        return
    }else if (name.length < 3) {
        formError.innerText = 'Please enter minimum three characters in name field'
        formError.style.display = 'block'
        return
    }else if(nameFormat.test(name)===false){
        formError.innerText = 'Name should be only alphabetic character'
        formError.style.display = 'block'
        return
    }
    const mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if(email.length===0){
        formError.innerText = 'Email is required'
        formError.style.display = 'block'
        return
    } else if(mailFormat.test(email)===false){
        formError.innerText = 'Please enter a valid email address.'
        formError.style.display = 'block'
        return
    }

    if(password.length===0){
        formError.innerText = 'Password is required'
        formError.style.display = 'block'
        return
    }else if(password.length<8){
        formError.innerText = 'Please enter minimum eight characters in password field'
        formError.style.display = 'block'
        return
    }
    
    if(confirmPassword.length===0){
        formError.innerText = 'Confirm Password is required'
        formError.style.display = 'block'
        return
    } else if(confirmPassword!==password){
        formError.innerText = "Confirm Password and Password didn't match"
        formError.style.display = 'block'
        return
    }

    const userDetails={
        email:email,
        name:name,
        password:password
    }
    const logInObject={
        isLogIn:true,
        email:email
    }
    localStorage.setItem(email,JSON.stringify(userDetails))
    localStorage.setItem('isLogIn',JSON.stringify(logInObject))

    window.location.href='index.html'






})

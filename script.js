const categories = document.getElementById('categories')
const menClothing = document.getElementById('men-clothing')
const womenClothing = document.getElementById('women-clothing')
const jewelry = document.getElementById('jewelry')
const electronics = document.getElementById('electronics')
const loader = document.getElementById('loader')
const section = document.getElementById('section')
const logOutButton = document.getElementById('log-out')
const userName = document.getElementById('user-name')

logOutButton.addEventListener('click', () => {
    localStorage.removeItem('isLogIn')
    window.location.href = 'login.html'
})

window.addEventListener('load', () => {
    categories.style.display = 'none'
    document.body.style.display='block'
    if (localStorage.getItem('isLogIn')) {
        const logInData = localStorage.getItem('isLogIn')
        const logInInfo = JSON.parse(logInData)
        if (logInInfo.isLogIn === false) {
            window.location.href = 'login.html'
            return
        } else {
            const userData = JSON.parse(localStorage.getItem(logInInfo.email))
            userName.innerText = userData.name
        }
    } else {
        window.location.href = 'login.html'
        return
    }

    loader.style.display = 'block'
    fetch('https://fakestoreapi.com/products').then((resData) => {
        return resData.json()
    }).then((response) => {

        if (response.length === 0) {
            const noDataFound = document.createElement('h1')
            noDataFound.setAttribute('id', 'data-not-found')
            categories.style.display = 'none'
            loader.style.display = 'none'
            noDataFound.innerText = 'Sorry, no results found!'
            section.append(noDataFound)
        }

        for (let index = 0; index < response.length; index++) {
            const item = response[index]

            const cardDiv = document.createElement('div')
            const descriptionDiv = document.createElement('div')
            const ratingPTag = document.createElement('p')
            const pricePPTag = document.createElement('p')
            const titleH1Tag = document.createElement('H1')
            const ratingSpan = document.createElement('span')
            const priceSpan = document.createElement('span')
            const imageElement = document.createElement('img')

            cardDiv.setAttribute('class', 'card')
            imageElement.setAttribute('src', item.image)
            imageElement.setAttribute('alt', 'product-image')
            titleH1Tag.setAttribute('class', 'product-title')
            descriptionDiv.setAttribute('class', 'card-description')
            ratingPTag.setAttribute('class', 'rating')
            pricePPTag.setAttribute('class', 'price')

            const title = item.title.length < 50 ? item.title : item.title.slice(0, 47) + '...'
            priceSpan.innerText = item.price
            ratingSpan.innerText = item.rating.rate
            titleH1Tag.innerText = title
            pricePPTag.innerText = 'Price: $'
            pricePPTag.append(priceSpan)

            ratingPTag.innerText = 'Rating: '
            ratingPTag.append(ratingSpan)
            descriptionDiv.append(titleH1Tag)
            descriptionDiv.append(ratingPTag)
            descriptionDiv.append(pricePPTag)
            cardDiv.append(imageElement)
            cardDiv.append(descriptionDiv)

            if (item.category === "men's clothing") {
                menClothing.children[1].append(cardDiv)
            } else if (item.category === "jewelery") {
                jewelry.children[1].append(cardDiv)
            } else if (item.category === "electronics") {
                electronics.children[1].append(cardDiv)
            } else if (item.category === "women's clothing") {
                womenClothing.children[1].append(cardDiv)
            }
            cardDiv.addEventListener('click', () => {
                localStorage.setItem('id', item.id)
                window.location.href = './product.html'
            })
        }
        categories.style.display = 'block'
        loader.style.display = 'none'
    }).catch((err) => {
        const errBody = document.createElement('h1')
        errBody.setAttribute('id', 'error')
        errBody.innerText = err.message
        categories.style.display = 'none'
        loader.style.display = 'none'
        section.append(errBody)

    })
}) 
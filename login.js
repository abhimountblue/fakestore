const form = document.getElementById('form')

window.addEventListener('load', () => {
    if (localStorage.getItem('isLogIn')) {
        const logInData = localStorage.getItem('isLogIn')
        const logInInfo = JSON.parse(logInData)
        if (logInInfo.isLogIn === true) {
            window.location.href = 'index.html'
            return
        }
    }
})

form.addEventListener('submit', (event) => {
    event.preventDefault()
    const password = document.getElementById('password').value
    const email = document.getElementById('email').value
    const formError = document.getElementById('form-error')
    formError.innerText = ''
    formError.style.display = 'none'
    formError.style.fontSize = '100%'

    if (email.length === 0) {
        formError.innerText = 'Email is required'
        formError.style.display = 'block'
        return
    }

    if (password.length === 0) {
        formError.innerText = 'Password is required'
        formError.style.display = 'block'
        return
    }
    if (localStorage.getItem(email)) {
        const userData = JSON.parse(localStorage.getItem(email))
        if (userData.password !== password) {
            formError.innerText = 'Email Id or Password is wrong'
            formError.style.display = 'block'
        } else {
            const logInObject = {
                isLogIn: true,
                email: userData.email
            }
            localStorage.setItem('isLogIn', JSON.stringify(logInObject))
            window.location.href = 'index.html'
        }
    } else {
        formError.innerText = 'User is not register with this email'
        formError.style.display = 'block'
    }


})
